loadIngredientsCheckbox();
loadRecipes();
loadIngredients();

function loadIngredientsCheckbox() {
    if ($("#ingredientsCheckbox") == null) {
        return;
    }
    console.log("Loading ingredients...");

    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "/request-handler");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.onload = function () {
        console.log("response received...");
        let ingredients = JSON.parse(xmlhttp.responseText);
        let ingredientsList = "";
        ingredients.forEach(ingredient => {
            ingredientsList += `<div id=${ingredient.id}>
            <label for="">${ingredient.name}</label>
            <input type="checkbox" name="ingredient">
        </div>`;
        });
        $("#ingredientsCheckbox").html(ingredientsList);
        console.log("ingredients loaded!");
    }

    let formData = {
        requestType: "loadIngredients"
    }
    xmlhttp.send(JSON.stringify(formData));
    console.log("sending request...");
}
function loadIngredients() {
    if ($("#ingredients") == null) {
        return;
    }
    console.log("Loading ingredients...");

    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "/request-handler");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.onload = function () {
        console.log("response received...");
        let ingredients = JSON.parse(xmlhttp.responseText);
        let ingredientsList = "";
        ingredients.forEach(ingredient => {
            ingredientsList += `<div class="ingredient" id=${ingredient.id}>
            <p>${ingredient.name}</p>
            <div onclick="deleteIngredient('${ingredient.id}')">Supprimer</div>
        </div>`;
        });
        $("#ingredients").html(ingredientsList);
        console.log("ingredients loaded!");
    }

    let formData = {
        requestType: "loadIngredients"
    }
    xmlhttp.send(JSON.stringify(formData));
    console.log("sending request...");
}
function loadRecipes() {
    if ($("#recipes") == null) {
        return;
    }
    console.log("Loading recipes...");

    let xmlhttpIngredients = new XMLHttpRequest();
    xmlhttpIngredients.open("POST", "/request-handler");
    xmlhttpIngredients.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttpIngredients.onload = function () {
        let ingredients = JSON.parse(xmlhttpIngredients.responseText);

        let xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "/request-handler");
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xmlhttp.onload = function () {
            console.log("response received...");
            let recipes = JSON.parse(xmlhttp.responseText);
            let recipesList = "";
            recipes.forEach(recipe => {
                console.log(recipe);
                if(recipe.imageUrl == null || recipe.imageUrl == ""){
                    recipe.imageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png";
                }
                recipesList += `
                <div class="recipe">
                    <p class="x" onclick="deleteRecipe('${recipe.name}')">X</p>
                    <h3>${recipe.name}</h3>
                    <img src="${recipe.imageUrl}"alt="">
                    <br>
                    <br>
                    <div>`;

                recipe.ingredients.forEach(recipeIngredient => {
                    let ingredientFound = ingredients.find(ingredient => ingredient.id == recipeIngredient);
                    recipesList += `<p>${ingredientFound.name}</p>`;
                });

                recipesList += `
                    </div>
                </div>
                `;
            });
            $("#recipes").html(recipesList);
            console.log("recipes loaded!");
        }

        let formData = {
            requestType: "loadRecipes"
        }
        xmlhttp.send(JSON.stringify(formData));
        console.log("sending request...");

    }

    let formDataIngredients = {
        requestType: "loadIngredients"
    }
    xmlhttpIngredients.send(JSON.stringify(formDataIngredients));
}
function addRecipe() {
    let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    xmlhttp.open("POST", "/request-handler");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.onload = function () {
        if (xmlhttp.responseText == "ok") {
            alert("Ajout réussi");
        } else {
            alert(xmlhttp.responseText);
        }
    }

    let recipeName = $("#recipename").val();
    let recipeImageUrl = $("#recipeImageUrl").val();
    if(recipeImageUrl == null){
        recipeImageUrl = "";
    }
    let ingredients = [];
    $("#ingredientsCheckbox input:checked").each(function () {
        ingredients.push($(this).parent().attr("id"));
    });

    console.log("Trying to add a recipe with name " + recipeName + " and ingredients ");
    console.log(ingredients);

    let formData = {
        requestType: "addRecipe",
        recipeName: recipeName,
        ingredients: ingredients,
        recipeImageUrl: recipeImageUrl
    }

    xmlhttp.send(JSON.stringify(formData));
}
function addIngredient() {
    let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    xmlhttp.open("POST", "/request-handler");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.onload = function () {
        if (xmlhttp.responseText == "ok") {
            alert("Ajout réussi");
        } else {
            alert(xmlhttp.responseText);
        }
    }

    let ingredientName = $("#ingredientName").val();
    console.log("Trying to add a ingredient with name " + ingredientName);

    let formData = {
        requestType: "addIngredient",
        ingredientName: ingredientName,
    }

    xmlhttp.send(JSON.stringify(formData));
}
function deleteRecipe(recipeName) {
    let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    xmlhttp.open("POST", "/request-handler");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.onload = function () {
        if (xmlhttp.responseText != "ok") {
            alert(xmlhttp.responseText);
        }
        loadRecipes();
    }

    let formData = {
        requestType: "deleteRecipe",
        recipeName: recipeName
    }

    xmlhttp.send(JSON.stringify(formData));
}
function deleteIngredient(ingredientId) {
    let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    xmlhttp.open("POST", "/request-handler");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.onload = function () {
        if (xmlhttp.responseText != "ok") {
            alert(xmlhttp.responseText);
        }
        loadIngredients();
    }

    let formData = {
        requestType: "deleteIngredient",
        ingredientId: ingredientId
    }

    xmlhttp.send(JSON.stringify(formData));
}