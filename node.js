const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
app.use('/css', express.static(__dirname + '/css'));
app.use('/js', express.static(__dirname + '/js'));
app.use(express.json());
const port = 3000;
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const xhr = new XMLHttpRequest();


app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});
app.get('/recipes', (req, res) => {
    res.sendFile(__dirname + '/recipes.html');
});
app.get('/ingredients', (req, res) => {
    res.sendFile(__dirname + '/ingredients.html');
});

app.post('/request-handler', (req, res) => {
    switch (req.body.requestType) {
        case "loadIngredients":
            jsonReader("ingredients.json", (err, ingredients) => {
                if (err) {
                    console.log(err);
                } else {
                    res.send(JSON.stringify(ingredients));
                }
            });

            break;
        case "loadRecipes":
            jsonReader("recipes.json", (err, recipes) => {
                if (err) {
                    console.log(err);
                } else {
                    res.send(JSON.stringify(recipes));
                }
            });
            break;
        case "addRecipe":
            jsonReader("recipes.json", (err, recipes) => {
                if (err) {
                    console.log(err);
                } else {

                    if (req.body.recipeName == null || req.body.recipeName == "") {
                        res.send("Le nom de la recette est obligatoire");
                        return;
                    }

                    let alreadyExists = false;
                    recipes.forEach(recipe => {
                        if (recipe.name == req.body.recipeName) {
                            alreadyExists = true;
                            return;
                        }
                    });

                    if (alreadyExists) {
                        res.send("Une recette avec ce nom existe déjà");
                        return;
                    }

                    if(req.body.ingredients == null || req.body.ingredients.length == 0) {
                        res.send("Vous devez choisir au moins un ingrédient");
                        return;
                    }

                    let recipe;

                    if(req.body.recipeImageUrl == null || req.body.recipeImageUrl == "") {
                        recipe = {
                            name: req.body.recipeName,
                            ingredients: req.body.ingredients
                        }
                    }else{
                        recipe = {
                            name: req.body.recipeName,
                            ingredients: req.body.ingredients,
                            imageUrl: req.body.recipeImageUrl
                        }
                    }

                    recipes.push(recipe);

                    fs.writeFile("recipes.json", JSON.stringify(recipes, null, 2), err => {
                        if (err) {
                            console.log(err)
                            res.send("Echec de l'ajout de la recette");
                            return;
                        }
                        res.send("ok");
                    })
                }
            });
            break;

        case "addIngredient":
            jsonReader("ingredients.json", (err, ingredients) => {
                if (err) {
                    console.log(err);
                } else {

                    if (req.body.ingredientName == null || req.body.ingredientName == "") {
                        res.send("Le nom de l'ingredient est obligatoire");
                        return;
                    }

                    let alreadyExists = false;
                    ingredients.forEach(ingredient => {
                        if (ingredient.name.toLowerCase() == req.body.ingredientName.toLowerCase()) {
                            alreadyExists = true;
                            return;
                        }
                    });

                    if (alreadyExists) {
                        res.send("Un ingredient avec ce nom existe déjà");
                        return;
                    }
                    let idList = []
                    ingredients.forEach(ingredient => {
                        idList.push(parseInt(ingredient.id))
                    })
                    let index = 1
                    while (idList.includes(index)) {
                        index += 1
                    }
                    let recipe = {
                        id: index.toString(),
                        name: req.body.ingredientName
                    }
                    ingredients.push(recipe);
                    ingredients.sort(function (a, b) {
                        return a.id > b.id;
                    });
                    fs.writeFile("ingredients.json", JSON.stringify(ingredients, null, 2), err => {
                        if (err) {
                            console.log(err)
                            res.send("Echec de l'ajout de l'ingredient'");
                            return;
                        }
                        res.send("ok");
                    })
                }
            });
            break;
        case "deleteRecipe":
            jsonReader("recipes.json", (err, recipes) => {
                if (err) {
                    console.log(err);
                } else {
                    let index = -1;
                    recipes.forEach((recipe, i) => {
                        if (recipe.name == req.body.recipeName) {
                            index = i;
                            return;
                        }
                    });

                    if (index == -1) {
                        res.send("La recette n'existe pas");
                        return;
                    }

                    recipes.splice(index, 1);

                    fs.writeFile("recipes.json", JSON.stringify(recipes, null, 2), err => {
                        if (err) {
                            console.log(err)
                            res.send("Echec de la suppression de la recette");
                            return;
                        }
                        res.send("ok");
                    })

                }
            });

            break;
        case "deleteIngredient":
            jsonReader("ingredients.json", (err, ingredients) => {
                if (err) {
                    console.log(err);
                } else {
                    let index = -1;
                    ingredients.forEach((ingredient, i) => {
                        if (ingredient.id == req.body.ingredientId) {
                            index = i;
                            return;
                        }
                    });

                    if (index == -1) {
                        res.send("L'ingredient n'existe pas");
                        return;
                    }


                    jsonReader("recipes.json", (err, recipes) => {
                        if (err) {
                            console.log(err);
                        } else {
                            recipes.forEach(recipe => {
                                recipe.ingredients.forEach((ingredient, i) => {
                                    if (ingredient == req.body.ingredientId) {
                                        recipe.ingredients.splice(i, 1);
                                    }
                                });
                            });

                            fs.writeFile("recipes.json", JSON.stringify(recipes, null, 2), err => {
                                if (err) {
                                    console.log(err)
                                    res.send("Echec de la suppression de l'ingredient");
                                    return;
                                }
                            })
                        }
                    });

                    ingredients.splice(index, 1);

                    fs.writeFile("ingredients.json", JSON.stringify(ingredients, null, 2), err => {
                        if (err) {
                            console.log(err)
                            res.send("Echec de la suppression de la recette");
                            return;
                        }
                        res.send("ok");
                    })

                }
            });

            break;
        default:
            res.send("received unknown request");
            break;
    }

});

app.listen(port, () => {
    console.log('Server started! At http://localhost:' + port);
});


function jsonReader(filePath, cb) {
    fs.readFile(filePath, 'utf-8', (err, fileData) => {
        if (err) {
            return cb && cb(err);
        }
        try {
            const object = JSON.parse(fileData);
            return cb && cb(null, object);
        } catch (err) {
            return cb && cb(err);
        }
    })
}

/*
jsonReader("", (err, data) => {
    if (err) {
        console.log(err)
    } else {
        fs.writeFile(`${file}`, JSON.stringify(data, null, 2), err => {
            if (err) {
                console.log(err)
            }
        })
    }
})
//*/
